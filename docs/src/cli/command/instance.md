# `lablie instance` command

The command `instance` creates SVG document containing instantiated template with provided data.

## Reference 

Output of the `lablie instance --help`: 

```
{{#include instance.help.txt}}
```

More detailed reference will be added in future.

## Related examples

Examples will be added in future.

